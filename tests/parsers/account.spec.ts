import { expect } from "chai";
import { RowDataPacket } from "mysql2";
import /* sinon */ { restore } from "sinon";
import { IAccountDB } from "../../src/models/account.db.model.js";
import { IAccount } from "../../src/models/account.model.js";
import AccountConverter from "../../src/parsers/account.parser.js"
import { mock_account_db_parser_arr, mock_account_parser_arr, mock_account_parser } from "../mock.data.js";

describe("account parser", function () {

  afterEach(() => {
    restore();
  })
  context("rowDataPacketToModel function", () => {

    it("should be a function", () => {
      // eslint-disable-next-line @typescript-eslint/unbound-method
      expect(AccountConverter.rowDataPacketToModel).to.be.instanceOf(Function);
    });

    it("should check if parser return IAccount obj from IAccount obj with the given params", () => {
      const row_data_result = AccountConverter.rowDataPacketToModel(mock_account_db_parser_arr as RowDataPacket[]);
      expect(row_data_result).to.eql(mock_account_parser_arr);
    });
  })

  context("accountModelToDb function", () => {

    it("should be a function", () => {
      // eslint-disable-next-line @typescript-eslint/unbound-method
      expect(AccountConverter.accountModelToDb).to.be.instanceOf(Function);
    });

    it("should check if parser return IAccountDB obj from IAccount obj with the given params", () => {
      const to_db_model_result = AccountConverter.accountModelToDb(mock_account_parser as IAccount);
      expect(to_db_model_result).to.eql(mock_account_parser as IAccountDB);
    });
  })
})

