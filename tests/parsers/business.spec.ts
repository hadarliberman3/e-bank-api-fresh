import {expect} from "chai";
import { RowDataPacket } from "mysql2";
import { restore } from "sinon";
import AgentConverter from "../../src/parsers/agent.parser.js"
import business_converter from "../../src/parsers/business.parser.js";
import {mock_business_db_parser_arr,mock_business} from "../mock.data.js";

describe("business parser", function() {

    afterEach(()=>{
        restore();
    })
    context("rowDataPacketToModel function",()=>{
  
      it("should be a function", () => {
        // eslint-disable-next-line @typescript-eslint/unbound-method
        expect(AgentConverter.rowDataPacketToModel).to.be.instanceOf(Function);
      });
  
      it("should check if parser return IAgent obj from RowDataPacket obj with the given params", () => {
        const row_data_result=business_converter.rowDataPacketToModel(mock_business_db_parser_arr as RowDataPacket[]);
        expect(row_data_result).to.eql(mock_business);
    });
})

})
  
  