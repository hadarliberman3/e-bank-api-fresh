/* eslint-disable @typescript-eslint/no-unsafe-call */
import { expect } from "chai";
import sinon, { restore } from "sinon";
import businessService from "../../src/services/business.service.js";
import addressRepository from "../../src/repositories/address.repository.js";
import businessRepository from "../../src/repositories/business.repository.js";
import accountRepository from "../../src/repositories/account.repository.js";
import business_converter from "../../src/parsers/business.parser.js";
import {ValidationService} from '../../src/services/logic.validation.service.js';
import { HttpException } from "../../src/exceptions/http.exception.js";
import { ADDRESS_ID, ACCOUNT_ID, mock_account, mock_business_db, mock_address_db, mock_business } from "../mock.data.js";
import { IAccountDB } from "../../src/models/account.db.model.js";


describe("Business service",  () => {


    // eslint-disable-next-line @typescript-eslint/require-await
    beforeEach(async function () {
        sinon.stub(businessRepository, "getBusinessesByIdDB").resolves(mock_business);
    });

    afterEach(function () {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        (businessRepository as any).getBusinessesByIdDB.restore();
        restore();
        // (businessService as any).getBusinessByID.restore();
    });

    context("create function", () => {
        it("should accept and return IBusinessAccount", async () => {
            sinon.stub(business_converter, "businessModelToDb").returns([mock_account as IAccountDB, mock_address_db, mock_business_db]);
            sinon.stub(addressRepository, "createAdressDB").resolves(ADDRESS_ID);
            sinon.stub(accountRepository, "createAccountDB").resolves(ACCOUNT_ID);
            sinon.stub(businessRepository, "createBusinessDB").resolves(ACCOUNT_ID);
            const result = await businessService.create(mock_business[0]);
            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(businessService.create).to.be.a("function");
            //@ts-ignore
            expect(result).to.eql(mock_business[0]);

        })
    });
    context("getBusinessByID function", () => {

        it("success when the business valid found and return IBusinessAccount", async () => {
            sinon.stub(ValidationService, "validateGetBusiness").returns(true);
            const result:any = await businessService.getBusinessByID(ACCOUNT_ID);
            // const result= await businessService.getBusinessByID(ACCOUNT_ID);
            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(businessService.getBusinessByID).to.be.a("function");
            //@ts-ignore
            expect(await result).to.eql(mock_business[0]);

        });

        it("throw error when the business is not valid",async () => {
            sinon.stub(ValidationService, "validateGetBusiness").returns(false);
            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(businessService.getBusinessByID).to.be.a("function");
            try {
                await businessService.getBusinessByID(ACCOUNT_ID);
            } catch (error) {
                expect((error as HttpException).message).to.be.equal("PLACEHOLDER FOR VALIDATION ERROR 1");
            }
        });
    })
})
