import {expect} from "chai";
// import sinon, { restore } from "sinon";
import {INPUT_VALIDATION_FUNCTIONS} from "../../src/utils/validation.utils.js";

describe("input validation", function() {

  context("isNumberBiTuples function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isNumberBiTuples).to.be.instanceOf(Function);
    });

    it("should check if array of tuples supllies and return true or false in adjusment to the boolean input", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isNumberBiTuples([[4,2],[5,6]],true)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumberBiTuples([["hi",2],[5,6]],true)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumberBiTuples([[4,2,3,4],[5,6]],true)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumberBiTuples("hi",true)).to.equal(false);
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isNumberBiTuples("hi","hi")).to.equal(false);
  });

  context("biTuplesSumGreaterThan function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesSumGreaterThan).to.be.instanceOf(Function);
    });
    
    it("should check if the sum of the array of tuples that supllies is greater than the second input number", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesSumGreaterThan([[4,2],[5,6]],1)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesSumGreaterThan([["hi",2],[5,6]],4)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesSumGreaterThan([[1,2],[3,4]],12)).to.equal(false);
    });
  });

  context("biTuplesAmountPositive function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesAmountPositive).to.be.instanceOf(Function);
    });
    
    it("should check if the tuples amount is positive and return true or false in adjusment to the boolean input", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesAmountPositive([[4,2],[5,6]],true)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesAmountPositive([["hi",2],[5,6]],true)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.biTuplesAmountPositive([[1,-20],[1,4]],true)).to.equal(false);
    });
  });
  
  context("isExist function",()=>{
  
    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isExist).to.be.instanceOf(Function);
    });

    it("should return true when both inputs types falsy", () => {
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isExist(undefined,false)).to.equal(true);
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isExist(0,false)).to.equal(true);
    });

    it("should return true when both inputs types truthy", () => {
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isExist(5,true)).to.equal(true);
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isExist("hi",true)).to.equal(true);
    });
    it("should return false when inputs types are different (falsy/truthy)", () => {
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isExist(undefined,true)).to.equal(false);
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.isExist("hi",false)).to.equal(false);
    });
  });

  context("isPositive function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive).to.be.instanceOf(Function);
    })

    it("should check if the first valur is positive number and return true or false in adjusment to the boolean input", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive(-1,false)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive(undefined,false)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive(5,true)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive("hi",true)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive(undefined,true)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.isPositive("hi",true)).to.equal(false);
    });     
  })

  context("isNumber function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isNumber).to.be.instanceOf(Function);
    });
  
    it("should check if first input is number and return true or false in adjusment to the boolean input", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isNumber(1,true)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumber(undefined,true)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumber(undefined,false)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumber(34,false)).to.equal(false);  
      });      
  })

  context("greaterThan function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.greaterThan).to.be.instanceOf(Function);
    });  

    it("should return true when first input is bigger than the second input", () => {
      //@ts-ignore
      expect(INPUT_VALIDATION_FUNCTIONS.greaterThan(2,1)).to.equal(true);
    });

    it("should return false when first input is smaller than the second input", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.greaterThan(1,2)).to.equal(false);
    });
         
  })

  context("numOfDigits function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.numOfDigits).to.be.instanceOf(Function);
    });

    it("should check if first value num of digits equal to the number in second value", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.numOfDigits(2000,4)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.numOfDigits(200,4)).to.equal(false);
    });       
  })

  context("isEmptyArray function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isEmptyArray).to.be.instanceOf(Function);
    });
  
    it("should return true if the first value is empty array and second is true", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isEmptyArray([],true)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isEmptyArray([1,2,3],false)).to.equal(true);
    });        
  })

  context("isIn function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isIn).to.be.instanceOf(Function);
    });

    it("should check if number is in the array and return boolean", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isIn(1,[1,2,3])).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isIn([2],[1,3,4])).to.equal(false);
    });     
  })

  context("isNumbersArray function",()=>{

    it("should be a function", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isNumbersArray).to.be.instanceOf(Function);
    });
  
    it("should check if the all elements in the array are numbers and return true or false in adjusment to the boolean input", () => {
      expect(INPUT_VALIDATION_FUNCTIONS.isNumbersArray([1,2,3],true)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumbersArray([2,3,4],false)).to.equal(false);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumbersArray(["hi",3,4],false)).to.equal(true);
      expect(INPUT_VALIDATION_FUNCTIONS.isNumbersArray(["hi",3,4],true)).to.equal(false);
    });     
  })
})
  })

