import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IBusinessAccountDB } from '../models/account.db.model.js';
import { IBusinessAccount } from '../models/account.model.js';
import BusinessConverter from '../parsers/business.parser.js';

class BusinessRepository {
  async createBusinessDB(business: IBusinessAccountDB): Promise<number> {
    const sql = 'INSERT INTO business_account SET ?';
    try {
      const rows = await connection.query(sql, business);
      const inserted_business_account_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return inserted_business_account_id;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getBusinessesByIdDB(accounts_id: number[]): Promise<IBusinessAccount[]> {
  
    try {
      const selected_data: RowDataPacket[] = [];
      await connection.beginTransaction();
      for (const account_id of accounts_id) {
        const sql = 
      'SELECT * FROM account as a ' +
      'INNER JOIN business_account as b ON b.account_info_id=a.account_id ' +
      'LEFT JOIN address as ad ON ad.address_id=b.address_id ' +
      'WHERE a.account_id = ?'
        const selected_account = (await connection.query(sql, account_id))[0];
        if ((selected_account as any[]).length > 0) {
          selected_data.push((selected_account as any[])[0] as RowDataPacket);
        }
      }
      await connection.commit();
      return BusinessConverter.rowDataPacketToModel(selected_data);
    } catch (err) {
      console.log(err);
      await connection.rollback();
      throw err;
    }
  }

}
export default new BusinessRepository();
