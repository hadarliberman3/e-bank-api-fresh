import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IFamilyAccountDB } from '../models/account.db.model.js';
import { IFamilyAccount, IIndividualAccount } from '../models/account.model.js';
import FamilyConverter from '../parsers/family.parser.js';
import IndividualRepository from './individual.repository.js';
import { FAMILY_ACCOUNT } from './select.models.js';

class FamilyRepository {
  async createFamilyDB(family: IFamilyAccountDB): Promise<void> {
    const sql = 'INSERT INTO family_account SET ?';
    try {
      await connection.query(sql, family);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async createFamilyWithOwnersDB(family: IFamilyAccountDB, owners_ids: number[]) {
    try {
      await connection.beginTransaction();
      await this.createFamilyDB(family);
      await this.addIndividualsToFamilyDB(owners_ids, family.account_info_id as number);
      await connection.commit();
    } catch (err) {
      console.log(err);
      await connection.rollback();
      throw err;
    }
  }

  async addIndividualsToFamilyDB(
    individuals_account_id: number[],
    family_account_id: number,
  ): Promise<IIndividualAccount[]> {
    try {
      let family_individual_array: [number, number][] = [];
      for (let id of individuals_account_id) {
        family_individual_array.push([family_account_id, id]);
      }
      const sql = 'INSERT INTO family_individual (family_id,individual_id)' + ' VALUES ?';
      await connection.query(sql, [family_individual_array]);
      const individuals_array: IIndividualAccount[] =
        await IndividualRepository.getIndividualsByIdDB(individuals_account_id);
      return individuals_array;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getFamilyWithOwnersByIdDB(family_account_id: number): Promise<IFamilyAccount[]> {
    const sql =
      `SELECT ${FAMILY_ACCOUNT} FROM family_account as f ` +
      'LEFT JOIN family_individual as fi ON fi.family_id=f.account_info_id ' +
      'LEFT JOIN individual_account as i ON i.account_info_id=fi.individual_id ' +
      'LEFT JOIN address as ad ON ad.address_id=i.address_id ' +
      'LEFT JOIN account as ai ON ai.account_id=i.account_info_id ' +
      'LEFT JOIN account as af ON af.account_id=f.account_info_id ' +
      'WHERE f.account_info_id=?';
    try {
      const [rows] = await connection.query(sql, family_account_id);
      console.log(rows);
      return FamilyConverter.rowDataPacketToModel(rows as RowDataPacket[]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  /*
  answer example
  [
  
     {
            "context": "acto pashido",
            "family_account_id": 4,
            "first_name": "yonatan",
            "last_name": "maya",
            "address_id": 1,
            "individual_account_id": 1,
            "email": "yooni@gmail.com",
            "person_id": 10223554,
            "individual_currency": "uk",
            "individual_balance": 6700,
            "individual_status": "active",
            "individual_agent": 1,
            "family_currency": "uk",
            "family_balance": 0,
            "family_status": "active",
            "family_agent": 1,
            "country_name": "israel",
            "country_code": "12",
            "postal_code": 33,
            "city": "haifa",
            "region": "lo",
            "street_name": "roth",
            "street_number": 4,
            "created_at": "2022-02-13T08:28:48.000Z",
            "updated_at": "2022-02-13T08:28:48.000Z"
        }
      
]*/

  // async getIndividualsByIdDB(individuals_id: number[]): Promise<IIndividualAccount[]> {
  //   const sql =
  //     `SELECT * FROM account as ai ` +
  //     'INNER JOIN individual_account as i ON i.account_info_id=ai.account_id ' +
  //     'INNER JOIN address as ad ON ad.address_id=i.address_id ' +
  //     `WHERE ai.account_id IN (${'?' + ',?'.repeat(individuals_id.length - 1)})`;

  //   try {
  //     const [rows] = await connection.query(sql, individuals_id);
  //     console.log("printing rows-----------------------------");
  //     (rows as RowDataPacket[]).forEach(console.log);
  //     return IndividualConverter.rowDataPacketToModel(rows as RowDataPacket[]);
  //   } catch (err) {
  //     console.log(err);
  //     throw err;
  //   }
  // }

  async removeIndividualsFromFamilyDB(
    individuals_account_id: number[],
    family_id: number,
  ): Promise<IIndividualAccount[]> {
    try {
      // let members_to_remove: [number, number][] = [];
      // for (let id of individuals_account_id) {
      //   members_to_remove.push([family_account_id, id]);
      // }
      // console.log(members_to_remove);
      const sql = 'DELETE FROM family_individual WHERE family_id = ? AND individual_id IN (?)';
      await connection.query(sql, [family_id, individuals_account_id]);
      const individuals_array: IIndividualAccount[] =
        await IndividualRepository.getIndividualsByIdDB(individuals_account_id);
      return individuals_array;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getPrimaryIdsToDeleteIndividualDB(
    family_account_id: number,
    individuals_account_id: number[],
  ): Promise<RowDataPacket[]> {
    const sql =
      'SELECT family_individual_id FROM family_individual ' +
      `WHERE family_id=? AND individual_id IN (${
        '?' + ',?'.repeat(individuals_account_id.length - 1)
      })`;
    try {
      const [rows] = await connection.query(sql, [family_account_id, ...individuals_account_id]);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}

export default new FamilyRepository();
