import fs from "fs";
import path from "path";
import chokidar from "chokidar"

let config:any = JSON.parse(fs.readFileSync(path.join(process.cwd(), "./config.json"), "utf-8"))
const watcher = chokidar.watch('./config.json').on('ready', function() {
});
watcher.on('change',()=>
{
    let {configurations,flags} = JSON.parse(fs.readFileSync(path.join(process.cwd(), "./config.json"), "utf-8"));
    config.configurations=configurations
    config.flags=flags
    console.log("config update!")
});
export default config;