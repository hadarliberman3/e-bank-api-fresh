import account_service from './account.service.js';
import transaction_repository from '../repositories/transaction.repository.js';
import { ITransactionDB, ITransferDB } from '../models/transaction.db.model.js';
import { getRate } from '../utils/transfer.utils.js';
import { ITransactionDTO } from '../models/requests.dto.model.js';
import { IAccount } from '../models/account.model.js';
import AccountService from './account.service.js';
import { ValidationService } from './logic.validation.service.js';
import {TransferType} from "../models/enums.js";
import businessRepository from '../repositories/business.repository.js';
import familyRepository from '../repositories/family.repository.js';

class TransferService {
  private async prepareTransfer(
    src_account: IAccount,
    dest_account: IAccount,
    amount: number,
    transaction_type: string,
  ): Promise<[ITransferDB,number]> {
    let rate = 1;

    //check if currencies are different
    if (src_account.currency !== dest_account.currency) {
      //then get rate
      rate = await getRate(src_account.currency, dest_account.currency);
    }

    //prepare tranfer db model
    const transaction: ITransactionDB = {
      src_id: src_account.account_id as number,
      dest_id: dest_account.account_id as number,
      transaction_type,
      amount,
    };
    const transfer_db_model: ITransferDB = {
      transaction,
      src_balance: src_account.balance - amount,
      dest_balance: dest_account.balance + amount * rate,
    };

    return [transfer_db_model,rate];
  }

  async createTransactionB2B(transaction_dto: ITransactionDTO): Promise<IAccount[]> {
    //get accounts
    const [src_account, dest_account] = await businessRepository.getBusinessesByIdDB([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    console.log({src_account});
    console.log({dest_account});

    ValidationService.validateTransferB2B(src_account, dest_account, transaction_dto.amount);
    
    // prepare transfer for db
    const [transfer_db_model] = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      TransferType.B2B,
    );

    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source, dest] = await AccountService.getAccountsByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);

    return [source,dest];
  }
  

  async createTransactionB2BFX(transaction_dto: ITransactionDTO): Promise<[IAccount[],number]> {
    //get accounts
    const [src_account, dest_account] = await businessRepository.getBusinessesByIdDB([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    ValidationService.validateTransferB2BFX(src_account, dest_account, transaction_dto.amount);
    
    // prepare transfer for db
    const [transfer_db_model,rate] = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      TransferType.B2B,
    );

    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source, dest] = await AccountService.getAccountsByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);

    return [[source, dest],rate];
  }


  async createTransactionB2I(transaction_dto: ITransactionDTO): Promise<IAccount[]> {
    const [src_account, dest_account] = await account_service.getAccountsByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);

    ValidationService.validateTransferB2I(src_account, dest_account, transaction_dto.amount);

    // prepare transfer for db
    const [transfer_db_model] = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      TransferType.B2I
    );

    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source, dest] = await AccountService.getAccountsByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);

    return [source, dest];
  }

  async createTransactionF2B(transaction_dto: ITransactionDTO): Promise<IAccount[]> {
    const [src_account] = await familyRepository.getFamilyWithOwnersByIdDB(transaction_dto.src);

    const [dest_account] = await businessRepository.getBusinessesByIdDB([transaction_dto.dest])

    ValidationService.validateTransferF2B(src_account, dest_account, transaction_dto.amount);

    // prepare transfer for db
    const [transfer_db_model] = await this.prepareTransfer(
      src_account,
      dest_account,
      transaction_dto.amount,
      TransferType.F2B
    );

    //execute transaction
    await transaction_repository.transaction(transfer_db_model);

    //return accounts
    const [source, dest] = await AccountService.getAccountsByID([
      transaction_dto.src,
      transaction_dto.dest,
    ]);

    return [source, dest];
  }
}

const transfer_service = new TransferService();
export default transfer_service;
