import { IAgent } from '../models/account.model.js';
import AgentRepository from '../repositories/agent.repository.js';
import { ValidationService } from './logic.validation.service.js';

class AgentService {
  async getAgentByAccessKey(access_key: string): Promise<IAgent> {
    const agents: IAgent[] = await AgentRepository.getAgentByAccessKey(access_key);
    ValidationService.validateGetAgent(agents);
    return agents[0];
  }
}

export default new AgentService();
