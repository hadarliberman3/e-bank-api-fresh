import AccountRepository from '../repositories/account.repository.js';
import { IAccount } from '../models/account.model.js';
import IndividualRepository from '../repositories/individual.repository.js';
import BusinessRepository from '../repositories/business.repository.js';
import { ValidationService } from './logic.validation.service.js';
import { AccountAction, AccountStatus } from '../models/enums.js';

class AccountService {
  async setStatus(account_type_tuples: [number, string][], action: string): Promise<IAccount[]> {
    const individual_accounts_ids: number[] = account_type_tuples
      .filter(account_type_tuple => account_type_tuple[1] === 'individual')
      .map(account_type_tuple => account_type_tuple[0]);
    const business_accounts_ids = account_type_tuples
      .filter(account_type_tuple => account_type_tuple[1] === 'business')
      .map(account_type_tuple => account_type_tuple[0]);

    const individual_accounts = await IndividualRepository.getIndividualsByIdDB(
      individual_accounts_ids,
    );
    const business_accounts = await BusinessRepository.getBusinessesByIdDB(business_accounts_ids);

    const all_accounts = [...individual_accounts, ...business_accounts];

    ValidationService.validateActiveDeactiveAccount(
      all_accounts,
      account_type_tuples.length,
      action,
    );

    const all_accounts_ids = [...individual_accounts_ids, ...business_accounts_ids];
    const newStatus =
      action === AccountAction.ACTIVATE ? AccountStatus.ACTIVE : AccountStatus.INACTIVE;
    await AccountRepository.updateStatusesAccountsByIdDB(all_accounts_ids, newStatus);

    all_accounts.forEach(account => (account.status = newStatus));
    return all_accounts;
  }

  async getAccountsByID(account_ids: number[]) {
    const accounts = await AccountRepository.getAccountsByIdDB(account_ids);
    return accounts;
  }
}

const account_service = new AccountService();
export default account_service;
