import express from 'express';
import BusinessController from '../controllers/business.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import { raw } from '../middlewares/common.middlewares.js';
import { inputValidationMiddleware } from '../middlewares/validation.middlewares.js';
import { Entities, ValidationRoutes } from '../models/enums.js';

const business_router = express.Router();

const entityValidator = inputValidationMiddleware(Entities.BUSINESS);

business_router.post('/', verifyAuth,entityValidator(ValidationRoutes.CREATE), raw(BusinessController.createBusiness));

business_router.get('/:id',
 verifyAuth,
 entityValidator(ValidationRoutes.GET),
  raw(BusinessController.getBusiness));

export default business_router;
