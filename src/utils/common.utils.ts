import { RequestHandler } from 'express';

export const curry = (
  f: (a: string, b: string) => RequestHandler,
): ((a: string) => (b: string) => RequestHandler) => {
  return (a: string): ((b: string) => RequestHandler) => {
    return (b: string): RequestHandler => {
      return f(a, b);
    };
  };
};
