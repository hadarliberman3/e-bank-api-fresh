import { AccountAction } from './enums';

export interface ICreateFamilyAccountDTO {
  currency: string;
  balance?: number;
  context?: string;
  owner_tuples: [owner_id: number, amount: number][];
}

export interface IAddRemoveFamilyMembersDTO {
  family_id: number;
  owner_tuples: [owner_id: number, amount: number][];
}

export interface ITransactionDTO {
  src: number;
  dest: number;
  amount: number;
}

export interface IChangeStatusDTO {
  action: AccountAction.ACTIVATE | AccountAction.DEACTIVATE;
  account_type_tuples: [number, string][];
}

// export interface IIdempotency {
//   idempotency_id: number|null;
//   idempotency_key: string,
//   agent_access_key:string,
//   request_hash:string,
//   response:any
// }
