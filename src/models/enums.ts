export enum ReponseFormat {
  SHORT = 'short',
  FULL = 'full',
}

export enum AccountAction {
  ACTIVATE = 'activate',
  DEACTIVATE = 'deactivate',
}

export enum ResponseStatus {
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

export enum AccountStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

export enum Entities {
  INDIVIDUAL = 'individual',
  FAMILY = 'family',
  BUSINESS = 'business',
  ACCOUNT = 'account',
}

export enum ValidationRoutes {
  CHANGE_ACCOUNT_STATUS = 'active_deactive_account',
  TRANSFER = 'transfer',
  CREATE = 'create_account',
  GET = 'get_account',
  ADD = 'add_members',
  REMOVE = 'remove_members',
  CLOSE = 'close_family',
}

export enum HttpStatusCode {
  OK = 200,
  CREATED = 201,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500,
  NETWORK_AUTHENTICATION_REQUIRED = 511,
}

export enum TransferType {
  B2B = "B2B",
  B2I = "B2I",
  F2B = "F2B",

}


