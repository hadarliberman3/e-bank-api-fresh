import { RowDataPacket } from 'mysql2';
import { IAccountDB, IFamilyAccountDB } from '../models/account.db.model.js';
import { IFamilyAccount, IIndividualAccount } from '../models/account.model.js';
import { ICreateFamilyAccountDTO } from '../models/requests.dto.model.js';
//import { IAccountDB, IFamilyAccountDB } from '../../models/account.db.model.js';
import AccountConverter from './account.parser.js';
import AddressConverter from './address.parser.js';

// "context": "acto pashido",
// "family_account_id": 4,
// "first_name": "yonatan",
// "last_name": "maya",
// "address_id": 1,
// family_type
// individual_type
// "individual_account_id": 1,
// "email": "yooni@gmail.com",
// "person_id": 10223554,
// "individual_currency": "uk",
// "individual_balance": 6700,
// "individual_status": "active",
// "individual_agent": 1,
// "family_currency": "uk",
// "family_balance": 0,
// "family_status": "active",
// "family_agent": 1,
// "country_name": "israel",
// "country_code": "12",
// "postal_code": 33,
// "city": "haifa",
// "region": "lo",
// "street_name": "roth",
// "street_number": 4,
// "created_at": "2022-02-13T08:28:48.000Z",
// "updated_at": "2022-02-13T08:28:48.000Z"

class FamilyConverter {
  familyDtoToModel(family_dto: ICreateFamilyAccountDTO): IFamilyAccount {
    //const balance = family_dto.owner_tuples.reduce((acc: number, curr) => acc + curr[1], 0)

    const family_model: IFamilyAccount = {
      context: family_dto.context as string,
      owners: family_dto.owner_tuples.map(tuple => tuple[0]) as unknown as IIndividualAccount[],
      currency: family_dto.currency,
      balance: 0,
      status: 'active',
      type: 'family',
    };

    return family_model;
  }

  familyDtoToDbModel(family_dto: ICreateFamilyAccountDTO): [IAccountDB, IFamilyAccountDB] {
    const family_model = this.familyDtoToModel(family_dto);

    const account_db_model = AccountConverter.accountModelToDb(family_model);

    const family_db_model: IFamilyAccountDB = {
      context: family_model.context,
      account_info_id: family_model.account_id,
    };

    return [account_db_model, family_db_model];
  }

  getPayments(
    owners_ammount: number[][],
    family_id: number,
    starting_family_balance: number,
  ): number[][] {
    let family_balance = starting_family_balance;
    const payments = owners_ammount.map(owner_payment => {
      const [src_id, src_ammount, src_balance] = owner_payment;
      const dest_id: number = family_id;
      const src_remaining_balance: number = src_balance - src_ammount;
      const dest_ammount: number = family_balance + src_ammount;
      family_balance += src_ammount;

      return [src_id, dest_id, src_remaining_balance, dest_ammount];
    });

    return payments;
  }

  rowDataPacketToModel(db_result: RowDataPacket[]): IFamilyAccount[] {
    console.log(db_result);

    if (db_result.length === 0) {
      return [];
    }

    const family: IFamilyAccount = db_result.reduce(
      (family_acc: IFamilyAccount, rowData) => {
        //address details
        const [address] = AddressConverter.rowDataPacketToModel([rowData] as RowDataPacket[]);

        //individual details
        const {
          individual_account_id,
          email,
          person_id,
          first_name,
          last_name,
          individual_type,
          individual_currency,
          individual_balance,
          individual_status,
          individual_agent,
        } = rowData;

        //family detals
        const {
          context,
          family_type,
          family_account_id,
          family_currency,
          family_balance,
          family_status,
          family_agent,
        } = rowData;

        if (person_id) {
          const owner: IIndividualAccount = {
            account_id: individual_account_id,
            individual_id: person_id,
            first_name,
            last_name,
            address,
            email,
            currency: individual_currency,
            balance: individual_balance,
            status: individual_status,
            agent_id: individual_agent,
            type: individual_type,
          };

          family_acc.owners.push(owner);
        }
        //family details
        family_acc.account_id = family_account_id;
        family_acc.context = context;
        family_acc.type = family_type;
        family_acc.currency = family_currency;
        family_acc.balance = family_balance;
        family_acc.status = family_status;
        family_acc.agent_id = family_agent;
        return family_acc;
      },
      {
        owners: [],
      } as unknown as IFamilyAccount,
    );

    return [family];
  }
}

const family_converter = new FamilyConverter();

export default family_converter;
