import { NextFunction, Request, RequestHandler, Response } from 'express';
import AgentService from '../services/agent.service.js';
import crypto from 'crypto';
import { HttpException } from '../exceptions/http.exception.js';
import { IAgent } from '../models/account.model.js';

export const verifyAuth: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<void> => {
  const { method: http_method } = req;
  const {
    'access-key': access_key,
    salt,
    timestamp,
    signature,
  } = req.headers as { 'access-key': string; salt: string; timestamp: string; signature: string };
  if (!access_key || !salt || !timestamp || !signature) {
    next(
      new HttpException(
        404,
        'Either access, salt, timestamp or signature were not be provided',
        null,
      ),
    );
  }
  let agent;
  try {
    agent = await AgentService.getAgentByAccessKey(access_key);
  } catch (err) {
    next(new HttpException(404, 'Place holder for auth Error', null));
  }

  const secret_key = (agent as IAgent).secret_key;

  if (!secret_key) {
    next(new HttpException(404, 'Place holder for auth Error', null));
  }

  let body: string = '';
  if (JSON.stringify(req.body) !== '{}' && req.body !== '') {
    body = JSON.stringify(req.body);
  }
  const to_sign = http_method + salt + timestamp + access_key + secret_key + body;
  let generated_signature = crypto.createHmac('sha256', secret_key).update(to_sign).digest('hex');
  if (generated_signature === signature) {
    next();
  } else {
    next(new HttpException(404, 'Place holder for auth Error signature', null));
  }
};
