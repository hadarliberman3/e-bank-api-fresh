import log from '@ajar/marker';
import app from './config/app.js';
import config from './config/config.js';
const { PORT, HOST } = config.configurations;


export function hello():number{
  return 1;
}

(() => {
  try {
    app.listen(Number(PORT), HOST as string);
    log.magenta(`api is live on ✨ ⚡  http://${HOST as string}:${Number(PORT)} ✨⚡`);
  } catch (err) {
    log.red(err);
  }
})();