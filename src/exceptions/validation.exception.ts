import { HttpException } from './http.exception.js';

export class ValidationException extends HttpException {
  constructor(public status: number, public message: string, public payload: unknown) {
    super(status, message, payload);
  }
}
