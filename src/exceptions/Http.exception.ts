export class HttpException extends Error {
  constructor(public status: number, public message: string, public payload: unknown) {
    super(message);
  }
}
