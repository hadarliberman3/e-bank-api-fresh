import { RequestHandler } from 'express';
import { IBusinessAccount } from '../models/account.model.js';
import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import business_service from '../services/business.service.js';

class BusinessController {
  createBusiness: RequestHandler = async (req, res) => {
    const business_to_create: IBusinessAccount = req.body;
    const response_data: IBusinessAccount = await business_service.create(business_to_create);
    res.status(HttpStatusCode.CREATED).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.CREATED,
      message: 'Business account was created successfully',
      data: response_data,
    });
  };

  getBusiness: RequestHandler = async (req, res) => {
    const business_id = Number(req.params.id);
    const response_data = await business_service.getBusinessByID(business_id);
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Business account found',
      data: response_data,
    });
  };
}

export default new BusinessController();
