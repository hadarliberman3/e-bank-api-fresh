import { RequestHandler } from 'express';
import { IAccount } from '../models/account.model.js';
import { AccountAction, HttpStatusCode, ResponseStatus } from '../models/enums.js';
import { IChangeStatusDTO, ITransactionDTO } from '../models/requests.dto.model.js';
import AccountService from '../services/account.service.js';
import TransferService from '../services/transaction.service.js';

class AccountController {
  setStatus: RequestHandler = async (req, res) => {
    const account_type_tuples: [number, string][] = (req.body as IChangeStatusDTO)
      .account_type_tuples;
    const action: AccountAction.ACTIVATE | AccountAction.DEACTIVATE = (req.body as IChangeStatusDTO)
      .action;
    const updated_accounts = await AccountService.setStatus(account_type_tuples, action);
    const response_data = updated_accounts.map(account => {
      return {
        id: account.account_id,
        status: account.status,
      };
    });

    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Status updated successfully',
      data: response_data,
    });
  };

  createTransferB2BFX: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const [src_dest,rate] = await TransferService.createTransactionB2BFX(transaction_request);
    const [src,dest] = src_dest.map(account => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    const response_data = {
      src,
      dest,
      rate 
    }

    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Transaction was made successfully',
      data: response_data,
    });
  };

  createTransferB2B: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const src_dest = await TransferService.createTransactionB2B(transaction_request);
    const response_data = src_dest.map((account : IAccount) => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Transaction was made successfully',
      data: response_data,
    });
  };






  createTransferB2I: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const src_dest = await TransferService.createTransactionB2I(transaction_request);
    const response_data = src_dest.map(account => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Transaction was made successfully',
      data: response_data,
    });
  };

  createTransferF2B: RequestHandler = async (req, res) => {
    const transaction_request: ITransactionDTO = req.body;
    const src_dest: IAccount[] = await TransferService.createTransactionF2B(transaction_request);
    const response_data = src_dest.map(account => {
      return {
        id: account.account_id,
        balance: account.balance,
        currency: account.currency,
      };
    });
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Transaction was made successfully',
      data: response_data,
    });
  };
}

export default new AccountController();
