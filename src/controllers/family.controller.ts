import { RequestHandler } from 'express';
import {
  IAddRemoveFamilyMembersDTO,
  ICreateFamilyAccountDTO,
} from '../models/requests.dto.model.js';
import family_service from '../services/family.service.js';
import { HttpStatusCode, ReponseFormat, ResponseStatus } from '../models/enums.js';
import { IFamilyAccount, IIndividualAccount } from '../models/account.model.js';
import { IGeneralObject } from '../modules/validation/validation.model.js';
class FamilyController {
  createFamily: RequestHandler = async (req, res) => {
    const family_to_create: ICreateFamilyAccountDTO = req.body;
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family_data = await family_service.create(family_to_create);
    let response_data: IGeneralObject = { ...family_data };
    if (format === ReponseFormat.SHORT) {
      response_data.owners = (response_data.owners as IIndividualAccount[]).map(
        owner => owner.account_id,
      );
    }
    res.status(HttpStatusCode.CREATED).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.CREATED,
      message: 'Family account was created successfully',
      data: response_data,
    });
  };

  getFamily: RequestHandler = async (req, res) => {
    const family_id: number = Number(req.params.id);
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family_data = await family_service.getByID(family_id);
    let response_data: IGeneralObject = { ...family_data };
    if (format === ReponseFormat.SHORT) {
      response_data.owners = (response_data.owners as IIndividualAccount[]).map(
        owner => owner.account_id,
      );
    }
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Family account found',
      data: response_data,
    });
  };

  closeFamily: RequestHandler = async (req, res) => {
    const family_id = Number(req.params.id);
    const family_data: IFamilyAccount = await family_service.closeFamily(family_id);
    let response_data: IGeneralObject = { ...family_data };
    if (req.query.format === ReponseFormat.SHORT) {
      response_data.owners = family_data.owners.map(owner => owner.account_id);
    }
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Family account is now closed',
      data: response_data,
    });
  };

  removeMembers: RequestHandler = async (req, res) => {
    const remove_request: IAddRemoveFamilyMembersDTO = req.body;
    const { family_id, owner_tuples } = remove_request;
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family_data: IFamilyAccount = await family_service.removeMembers(family_id, owner_tuples);
    const response_data: IGeneralObject = family_data;
    if (format === ReponseFormat.SHORT) {
      response_data.owners = (response_data.owners as IIndividualAccount[]).map(
        owner => owner.account_id,
      );
    }
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Memebers removed successfully',
      data: response_data,
    });
  };

  addMembers: RequestHandler = async (req, res) => {
    const add_request: IAddRemoveFamilyMembersDTO = req.body;
    const { family_id, owner_tuples } = add_request;
    const format =
      req.query.format === ReponseFormat.SHORT ? ReponseFormat.SHORT : ReponseFormat.FULL;
    const family_data: IFamilyAccount = await family_service.addMembers(family_id, owner_tuples);
    const response_data: IGeneralObject = family_data;
    if (format === ReponseFormat.SHORT) {
      response_data.owners = (response_data.owners as IIndividualAccount[]).map(
        owner => owner.account_id,
      );
    }
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Memebers added successfully',
      data: response_data,
    });
  };
}

export default new FamilyController();
